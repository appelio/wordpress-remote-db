#!/bin/bash

# Fetch WordPress
wget https://wordpress.org/latest.tar.gz
tar -xzf latest.tar.gz

# Prepare the DB
#mysql -f -u root -pchangeme <<DBSCRIPT
#CREATE USER 'wordpress-user'@'localhost' IDENTIFIED BY 'changeme';
#CREATE DATABASE wordpress;
#GRANT ALL PRIVILEGES ON wordpress.* TO 'wordpress-user'@'localhost';
#FLUSH PRIVILEGES;
#DBSCRIPT

# Configure Wordpress
cp wordpress/wp-config-sample.php wordpress/wp-config.php
sed -i -e "s/define( 'DB_NAME', 'database_name_here' );/define( 'DB_NAME', 'wpdb3' );/" wordpress/wp-config.php
sed -i -e "s/define( 'DB_USER', 'username_here' );/define( 'DB_USER', 'admin' );/" wordpress/wp-config.php
sed -i -e "s/define( 'DB_PASSWORD', 'password_here' );/define( 'DB_PASSWORD', 'changeme' );/" wordpress/wp-config.php
sed -i -e "s/localhost/rds.coh19lckqzph.eu-west-1.rds.amazonaws.com/g" /wordpress/wp-config.php
cp -r wordpress/* /var/www/html/

#cp /etc/httpd/conf/httpd.conf /etc/httpd/conf/httpd.conf.bak
#awk '/AllowOverride None/{c++;if(c==2){sub("AllowOverride None","AllowOveride All");c=0}}1' /etc/httpd/conf/httpd.conf | tee  httpd.conf.temp
#cp httpd.conf.temp /etc/httpd/conf/httpd.conf
systemctl restart httpd
